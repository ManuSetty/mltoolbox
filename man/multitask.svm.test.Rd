\name{multitask.svm.test}
\alias{multitask.svm.test}
\title{Multitask SVM based on formulation in Evgneiou and Pontil (2004). This function uses
LiblineaR for SVM computation}
\usage{
  multitask.svm.test(multitask.svm.res, feature.matrices,
    labels)
}
\arguments{
  \item{multitask.svm.res}{Output from
  \code{multitask.svm.train}}

  \item{feature.matrices}{List of feature matrices with a
  matrix for each task}

  \item{labels}{List of training label vectors}
}
\value{
  \item{test.*}{Test accuracies, predictions, aucs}
}
\description{
  Multitask SVM based on formulation in Evgneiou and Pontil
  (2004). This function uses LiblineaR for SVM computation
}

