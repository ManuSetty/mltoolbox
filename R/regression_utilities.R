# Functions for regression utilities
# Manu N Setty
# 11/14/2009

# Revised           Comments
# 02/11/2010        Added crossvalidation function
# 02/12/2010        Book keeping in crossvalidation function
# 03/15/2010        Added group lasso function


# Fraction of variance explained
# Wikipedia
# R^2 = SSreg / SStot
# SStot = SSreg + SSerr
# SSreg = Regression sum of squares
# SSerr = Residual sum of squares

#' Fraction of variance explained
#' @export
variance.exp <- function (y, ypre) {
  SSerr = sum ((y - ypre)^2)
  SSreg = sum ((mean(ypre) - ypre)^2)

  return (SSreg / (SSreg + SSerr))
}

#' Glmnet crossvalidation
#' @export

crossvalidation <- function (data.vector, feature.matrix, lambda, cv.folds, method="glmnet", ...) {

  if (nrow (feature.matrix) != length (data.vector))
    stop ("data vector length should be same as number of rows of the feature matrix")

  if (length (setdiff (1:nrow (feature.matrix), unlist (cv.folds))) != 0 )
    warning ("Cross validation folds do not contain all the observations")
 
  # Determine lambda sequence by running on the whole dataset
  if (!hasArg (lambda))
    lambda <- func (data.vector, feature.matrix, ...)$lambda
  
  # Y:  Number of explanatory variables
  Y <- length (data.vector)
  no.folds <- length (cv.folds)

  ## Convert to matrix if feature matrix is a dataframe
  feature.matrix <- as.matrix (feature.matrix)
  
  # Run folds
  ypre <- matrix (0, nrow = Y, ncol=length(lambda))
  correlations <- c ()
  for (fold in 1:no.folds) {
      holdout.ind <- cv.folds[[fold]][ cv.folds[[fold]] <= Y ]
      training.ind <- setdiff (1:Y, holdout.ind)

      ## Training using glmnet
      res <- glmnet (feature.matrix[-holdout.ind,], data.vector[holdout.ind,], lambda=lambda, ...)
      ypre[holdout.ind, ] <- predict (res, feature.matrix[holdout.ind,])
      correlations <- cbind (correlations, cor (ypre[holdout.ind,], data.vector[holdout.ind], method='s'))
      gc ()
    }

  ## Evaluate regression by means of error
  err <- apply (ypre, 2, function (x) { sum ((x - data.vector) ^ 2)})/length (data.vector)
  correlations <- rowMeans (correlations, na.rm=TRUE)
  min.error.ind = which (err == min (err))
  return ( list( ypre=ypre, min.error.ind = min.error.ind, min.error=min(err),
                lambda=lambda, correlation=correlations[min.error.ind] ))
}
