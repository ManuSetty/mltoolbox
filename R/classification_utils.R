
## Script with classification related utilities
## Manu Setty
## 06/14/2012

## Revised        Comments
## 12/11/2012     Minor bug fix in auc determination
## 02/26/2013     Bug fix in get.auc: Changed "end" variable setting from length (labels) to length (flags)
## 02/27/2013     get.auc:  Ranks for predictions changed to use ties.method to <random>


#' Unit normalize each example
#'
#' @export
unit.normalize <- function (x) {
  n.factors <- sqrt (rowSums (x ^ 2)) + 1e-10
  return (x/n.factors)
}

#' Determine area under ROC
#' @param labels +1/-1 vector of labels
#' @param predictions Margin/Prediction scores
#' @param plot Logical to determine whether ROC should be plotted. Default is \code{FALSE}
#' @param ... Additional arguments to plot function
#' @return \item{auc}{Area under ROC}
#' \item{truepos}{True positive rate}
#' \item{falsepos}{False positive rate}
#' 
#' @export

get.auc <- function (labels, predictions, plot=FALSE, ...) {

  if (!all (labels %in% c(1, -1)))
    stop ('Labels have to +1/-1')
  
  ## Ranks for predictions
  sign.preds <- sign (predictions)
  ranks <- rank (predictions, ties.method='random')
  preds <- ranks[c(which (labels==1), which (labels==-1))]
  labels <- sort (labels, decreasing=TRUE)
  labels[labels == -1] <- 0

  ## Sorted order
  inds <- sort (preds, decreasing=TRUE, index.return=TRUE)$ix
  preds <- preds[inds]
  labels <- labels[inds]
  sign.preds <- sign.preds[inds]

  ## Determine true and false positive rates
  truepos <- cumsum (labels)
  falsepos <- 1:length (predictions) - truepos
  flags <- c(diff(predictions), 1) != 0
  truepos <- truepos[flags]/length (which (sign.preds == 1 & labels == 1 | sign.preds != 1 & labels == 1))
  falsepos <- falsepos[flags]/length (which (sign.preds == 1 & labels == 0 | sign.preds != 1 & labels == 0))

  ## Calculate auc
  end <- length (which (flags))
  auc <- sum((falsepos[2:end] - falsepos[1:(end-1)]) *
             (truepos[2:end] + truepos[1:(end-1)])/2)

  ## Plot
  if (plot) {
    plot (falsepos, truepos, type='l', xlim=c(0, 1), ylim=c(0, 1), ...,
          xlab='False positive rate', ylab='True positive rate')
    lines (c(-10, 10), c(-10, 10), col='grey', lty=2)
  }
  
  res <- list()
  res$auc <- auc; res$truepos <- truepos; res$falsepos <- falsepos
  return (res)
}


#' Determine classification accuracy
#' @param labels +1/-1 vector of labels
#' @param preds Margin/Prediction scores
#'
#' @export

get.accuracy <- function (labels, preds) 
  return (length (which (labels == sign (preds)))/length (preds))


#' Plot collection of ROCs
#' @param auc.res.list List of results from \code{get.auc}
#' @param col Colors for ROC.
#' @param plot.new Logical to indicate if the curves should be a new plot. Defaults to \code{TRUE}.
#'
#' @export

plot.aucs <- function (auc.res.list, col, plot.new=TRUE, ...) {

  ## Set up
  if (plot.new)
    plot (0, type='n', xlab='False positive rate', ylab='True positive rate',
          xlim=c(0, 1), ylim=c(0, 1))
  for (i in 1:length (auc.res.list))
    lines (auc.res.list[[i]]$falsepos, auc.res.list[[i]]$truepos, col=col[i], lwd=2, ...)
  lines (c(-10, 10), c(-10, 10), col='grey', lty=2)

  ## Legend
  if (plot.new)
    legend ('bottomright', names (auc.res.list), bty='n',
            lty=1, col=colors.list[1:length (auc.res.list)])
}
            

